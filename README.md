# Weather Wave Module

- A 3-Day Hourly Weather Forecast Module that provides a customized live weather
  report as a block using weather API.

## Table of contents

- Introduction
- Description
- Requirements
- Installation
- Configuration
- Maintainers

## Introduction

- The "3-Day Hourly Weather Forecast Module" is a powerful and 
  user-friendly weather display tool designed to provide 
  detailed weather information for a specific location 
  over the course of three days. This custom module offers a 
  comprehensive hourly breakdown of weather conditions, 
  ensuring users stay informed and prepared for any 
  upcoming weather changes.The module is designed to be 
  responsive and compatible with various devices, including
  desktops, tablets, and mobile phones, ensuring accessibility 
  for all users.

## Description

- It is a module that provides a customized live weather
  report that needs to be placed in a block to
  /admin/structure/block.
- Enter the name of the city for which you want
  to get the weather report and then you
  will get the weather details of the the current
  weather as well as the details of 24 hours weather.
- Also, It has the feature to choose the option for 
  Today, Tomorrow and Day After tomorrow. 
  Below the current weather display you will get the
  details of whole 24 hours weather.
   
## Requirements

- This module requires no modules outside of Drupal core.

## Installation

- Install this module as you would normally install
  a contributed Drupal module.
  [Visit](https://www.drupal.org/node/1897420) for further information.

## Configuration

- Once the module has been installed, navigate to
  /admin/config/weather/settings
  (Configuration > Web Services > Weather Wave Configuration )
  and configure the locations.

## Maintainers
- Prachi Jain - [prachi6824](https://www.drupal.org/u/prachi6824)
- Sonam Sharma - [sonam_sharma](https://www.drupal.org/u/sonam_sharma)
- Vijay Sharma - [vijaysharma_89](https://www.drupal.org/u/vijaysharma_89)


