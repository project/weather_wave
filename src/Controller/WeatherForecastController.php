<?php

namespace Drupal\weather_wave\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use GuzzleHttp\Client;

class WeatherForecastController extends ControllerBase  {
  /**
   * Callback function for city autocomplete.
   */
  public function renderBlockContent(Request $request) {
    // Making an API request to fetch city suggestions based on $input.
    $input = !empty($request->query->get('q'))? $request->query->get('q') : 'indore';
    $suggestions = [];
    $city_api ='http://api.openweathermap.org/geo/1.0/direct?q='.$input.'&limit=5&appid=e618f17cfe11879305a07e34043cb52b';
    $client_city_api = \Drupal::httpClient()->get($city_api);
    // Use Drupal's HTTP client to make the API request.
    $city_name_api = json_decode($client_city_api->getBody(),True);
    if (!empty($city_name_api)) {
      foreach ($city_name_api as $city) {
        $suggestions[] = [
          'value' => $city['name'] . ',' . $city['state'] . ',' . $city['country'],
          'label' => $city['name'] . ',' . $city['state'] . ',' . $city['country'],
        ];
      } 
      return new JsonResponse($suggestions);
    }
  }
}