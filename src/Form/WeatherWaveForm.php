<?php

namespace Drupal\weather_wave\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *  Class WeatherWaveForm
 */

Class WeatherWaveForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return[
      'weather_wave.WeatherWave',
    ];
  }
  /**
   * {@inheritdoc}
   */
  public function getFormId(){

    return 'weather_wave_form';
    
  }
  /**
   * {@inheritdoc}  
   */ 
  public function buildForm(array $form, FormStateInterface $form_state){
   
    $config = $this->config('weather_wave.WeatherWave');
    $defualtvalue = !empty($config->get('city'))? $config->get('city') : 'indore';
    $days = !empty($config->get('days'))? $config->get('days') : 'select';
    $form['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter City Name'),
      '#placeholder' => 'Enter City Name',
      '#autocomplete_route_name' => 'weather_wave.autocomplete',
      '#default_value' => $defualtvalue,
      '#attributes' => [
        'class' => ['form-control'],
      ],
    ];
    $form['days'] = [
      '#type' => 'select',
      '#title' => $this->t('Days'),
      '#options' => [
        'today' => $this->t('Today'),
        'tomorrow' => $this->t('Tomorrow'),
        'day_after_tomorrow' => $this->t('Day After Tomorrow'),
      ],
      '#required' => False, // Make the field required if needed.
      '#default_value' => $days,
    ];

    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => t('Reset'),
      '#submit' => ['::weather_wave_reset_form_submit'], // Define a custom submit handler.
      '#weight' => 100,
      '#attributes' => [
        'class' => ['button--primary'],
      ],
    ]; 
    return parent::buildForm($form, $form_state);
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormstateInterface $form_state){
    $form_value = $form_state->getValue('key');
    $city = !empty($form_state->getValue('city')) ? $form_state->getValue('city'): "indore";
    $article_id = EntityAutocomplete::extractEntityIdFromAutocompleteInput($city);
    $days = $form_state->getValue('days');
    parent::submitForm($form, $form_state);
    $this->config('weather_wave.WeatherWave')
         ->set('city',$city)
         ->set('days',$days)
         ->save();
  }
  /**
   * Custom submit handler for the reset button.
   */
  function weather_wave_reset_form_submit(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    // Reset the value of the city field to its default value.

    $cityFieldValue = \Drupal::config('weather_wave.WeatherWave')->get('city');
    $reset = NULL;
    $this->config('weather_wave.WeatherWave')
    ->set('city',$reset)->save();
  }
}