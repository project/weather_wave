<?php 
/**
 * @file
 * Contains \Drupal\weather_wave\Plugin\Block\WeatherWaveBlock.
 */

 namespace Drupal\weather_wave\Plugin\Block;

 use Drupal\Core\Block\BlockBase;
 use Drupal\Core\Form\FormStateInterface;
 use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
 use Symfony\Component\DependencyInjection\ContainerInterface;
 use Drupal\Core\Config\ConfigFactoryInterface;
 use Drupal\Component\Utility\Html;
 use Drupal\Core\Config\Entity\ConfigEntityInterface;
 
/**
 * Provides a 'Weather Wave' Block.
 * 
 * @Block(
 *   id = "weather_wave_block",
 *   admin_label = @Translation("Weather Wave Block"),
 *   category = @Translation("Weather Block"),
 * )
 */
class WeatherWaveBlock extends BlockBase{

  /**
   * The Drupal configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a location form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param Drupal\user\UserDataInterface $user_data
   *   The user data service.
   */

  /**
   * {@inheritdoc}
   */

  public function build() {
  global $base_url;
    
    $build = [];
    $build['#form'] = \Drupal::formBuilder()->getForm('\Drupal\weather_wave\Form\WeatherWaveForm');
    $city = !empty(\Drupal::config('weather_wave.WeatherWave')->get('city')) ? \Drupal::config('weather_wave.WeatherWave')->get('city') : 'indore';
    $module_path = \Drupal::service('module_handler');
    $module_path = $module_path->getModule('weather_wave')->getPath();
    $path = $base_url . '/' . $module_path;
    $api = 'https://api.weatherapi.com/v1/forecast.json?q='.$city.'&days=7&key=8f2e8f9361f6463a80660632231107';
    $client_api = \Drupal::httpClient()->get($api);
    $response_api = json_decode($client_api->getBody(),True);
    $build['#theme'] = 'weather_forecast_template';
    $build['#path'] = $path;
    $build['#response_api'] =  $response_api;
    $build['#attached']['library'] = array('weather_wave/weather_style');
    $days = \Drupal::config('weather_wave.WeatherWave')->get('days');
    $build['#days'] =  $days;
    return $build;  
  } 
}